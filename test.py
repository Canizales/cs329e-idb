import os
import sys
import unittest

from create_animal import app, db, Animal,Biome, Class,Family, Order, Conservation


class animalTestCases(unittest.TestCase):
	def test_animal_insert(self):
		s = Animal(animal="panda", scientific_name = "idk", image="image.img", description="black and white",biome='Mars', biome_wiki_url='wiki.url', biome_desc='insertDesc', biome_img='image.img',phylum_type = "phylum_type", class_type = "class_type", order_type="order_type",family_type = 'family_type',conservation_status="status.txt",conservation_status_wiki_url="wiki.url",
		conservation_status_desc="insertDesc",conservation_status_img="image.img")
		db.session.add(s)
		db.session.commit()
		r = db.session.query(Animal).filter_by(animal = 'panda').one()
		self.assertEqual(str(r.animal), 'panda')
		r = db.session.query(Animal).filter_by(scientific_name = 'idk').one()
		self.assertEqual(str(r.scientific_name), 'idk')
		r = db.session.query(Animal).filter_by(image = 'image.img').one()
		self.assertEqual(str(r.image), 'image.img')
		r = db.session.query(Animal).filter_by(description = 'black and white').one()
		self.assertEqual(str(r.description), 'black and white')


	def test_biome_insert(self):
		s = Biome(biome_type="biome_type",biome_desc="biome_desc",biome_img="biome_img",animal="animal", average_temperature="average_temperature")
		db.session.add(s)
		db.session.commit()
		r = db.session.query(Biome).filter_by(biome_type = 'biome_type').one()
		self.assertEqual(str(r.biome_type), 'biome_type')
		r = db.session.query(Biome).filter_by(biome_desc = 'biome_desc').one()
		self.assertEqual(str(r.biome_desc), 'biome_desc')
		r = db.session.query(Biome).filter_by(biome_img = 'biome_img').one()
		self.assertEqual(str(r.biome_img), 'biome_img')
		r = db.session.query(Biome).filter_by(animal = 'animal').one()
		self.assertEqual(str(r.animal), 'animal')
		r = db.session.query(Biome).filter_by(average_temperature = 'average_temperature').one()
		self.assertEqual(str(r.average_temperature), 'average_temperature')

	def test_Class_insert(self):
		s = Class(class_type="class_type",class_desc="class_desc", class_wiki_url="class_wiki_url",class_img_url="class_img_url",parent_phylum="parent_phylum")
		db.session.add(s)
		db.session.commit()
		r = db.session.query(Class).filter_by(class_type = 'class_type').one()
		self.assertEqual(str(r.class_type), 'class_type')
		r = db.session.query(Class).filter_by(class_desc = 'class_desc').one()
		self.assertEqual(str(r.class_desc), 'class_desc')
		r = db.session.query(Class).filter_by(class_wiki_url = 'class_wiki_url').one()
		self.assertEqual(str(r.class_wiki_url), 'class_wiki_url')
		r = db.session.query(Class).filter_by(class_img_url = 'class_img_url').one()
		self.assertEqual(str(r.class_img_url), 'class_img_url')
		r = db.session.query(Class).filter_by(parent_phylum = 'parent_phylum').one()
		self.assertEqual(str(r.parent_phylum), 'parent_phylum')

	def test_Order_insert(self):
		s = Order(order_type="order_type",order_desc="order_desc", order_wiki_url="order_wiki_url", order_img_url="order_img_url", parent_class="parent_class")
		db.session.add(s)
		db.session.commit()
		r = db.session.query(Order).filter_by(order_type = 'order_type').one()
		self.assertEqual(str(r.order_type), 'order_type')
		r = db.session.query(Order).filter_by(order_desc = 'order_desc').one()
		self.assertEqual(str(r.order_desc), 'order_desc')
		r = db.session.query(Order).filter_by(order_wiki_url = 'order_wiki_url').one()
		self.assertEqual(str(r.order_wiki_url), 'order_wiki_url')
		r = db.session.query(Order).filter_by(order_img_url = 'order_img_url').one()
		self.assertEqual(str(r.order_img_url), 'order_img_url')
		r = db.session.query(Order).filter_by(parent_class = 'parent_class').one()
		self.assertEqual(str(r.parent_class), 'parent_class')
		

	def test_Family_insert(self):
		s = Family(family_type="family_type",family_desc="family_desc",family_wiki_url="family_wiki_url", family_img_url="family_img_url",parent_order = "parent_order")
		db.session.add(s)
		db.session.commit()
		r = db.session.query(Family).filter_by(family_type = 'family_type').one()
		self.assertEqual(str(r.family_type), 'family_type')
		r = db.session.query(Family).filter_by(family_desc = 'family_desc').one()
		self.assertEqual(str(r.family_desc), 'family_desc')
		r = db.session.query(Family).filter_by(family_wiki_url = 'family_wiki_url').one()
		self.assertEqual(str(r.family_wiki_url), 'family_wiki_url')
		r = db.session.query(Family).filter_by(family_img_url = 'family_img_url').one()
		self.assertEqual(str(r.family_img_url), 'family_img_url')
		r = db.session.query(Family).filter_by(parent_order = 'parent_order').one()
		self.assertEqual(str(r.parent_order), 'parent_order')

	def test_conserv_insert(self):
		s =  Conservation(conservation_status="conservation_status",conservation_status_wiki_url="conservation_status_wiki_url",
			conservation_status_desc="conservation_status_desc",conservation_status_img="conservation_status_img" , animal="animal" )
		db.session.add(s)
		db.session.commit()
		r = db.session.query(Conservation).filter_by(conservation_status = 'conservation_status').one()
		self.assertEqual(str(r.conservation_status), 'conservation_status')
		r = db.session.query(Conservation).filter_by(conservation_status_wiki_url = 'conservation_status_wiki_url').one()
		self.assertEqual(str(r.conservation_status_wiki_url), 'conservation_status_wiki_url')
		r = db.session.query(Conservation).filter_by(conservation_status_desc = 'conservation_status_desc').one()
		self.assertEqual(str(r.conservation_status_desc), 'conservation_status_desc')
		r = db.session.query(Conservation).filter_by(conservation_status_img = 'conservation_status_img').one()
		self.assertEqual(str(r.conservation_status_img), 'conservation_status_img')

	def test_json_insert_1(self):

		# test book table
		r = db.session.query(Animal).filter_by(animal = 'Coyote').one()
		self.assertEqual(str(r.scientific_name), "Canis latrans")
		self.assertEqual(str(r.biome_wiki_url), 'https://en.wikipedia.org/wiki/Desert')
		self.assertEqual(str(r.class_type), "Mammalia")
		self.assertEqual(str(r.order_type), 'Carnivora')

	def test_json_insert_2(self):
		# test book table
		r = db.session.query(Conservation).filter_by(conservation_status = 'Least Concern').one()

		self.assertEqual(str(r.conservation_status_wiki_url), "https://en.wikipedia.org/wiki/Least-concern_species")
		self.assertEqual(str(r.conservation_status_desc), "A least concern (LC) species is a species which has been categorized by the International Union for Conservation of Nature (IUCN) as evaluated as not being a focus of species conservation. They do not qualify as threatened, near threatened, or (before 2001) conservation dependent.")
if __name__ == '__main__':
	unittest.main()
