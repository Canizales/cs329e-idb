# beginning of models.py
# note that at this point you should have created "bookdb" database (see install_postgres.txt).
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

#create flask
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:2512488@localhost:5432/animaldb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

#create class for animal
class Animal(db.Model):
	__tablename__ = 'animal'

	animal = db.Column(db.String(80), nullable = False, primary_key = True)
	scientific_name = db.Column(db.String(254), nullable = False, primary_key = True)
	image = db.Column(db.String(2000), nullable =False, primary_key = True)
	description = db.Column(db.String(10000), nullable = False, primary_key = True)
	biome = db.Column(db.String(80), nullable = False, primary_key = True)
	biome_wiki_url = db.Column(db.String(2000), nullable =False, primary_key = True)
	biome_desc = db.Column(db.String(2000), nullable =False, primary_key = True)
	biome_img = db.Column(db.String(2000), nullable =False, primary_key = True)
	phylum_type = db.Column(db.String(80), nullable = False, primary_key = True)
	class_type = db.Column(db.String(80), nullable = False, primary_key = True)
	order_type = db.Column(db.String(80), nullable = False, primary_key = True)
	family_type = db.Column(db.String(80), nullable = False, primary_key = True)
	conservation_status = db.Column(db.String(80), nullable = False, primary_key = True)
	conservation_status_wiki_url = db.Column(db.String(2000), nullable =False, primary_key = True)
	conservation_status_desc = db.Column(db.String(2000), nullable =False, primary_key = True)
	conservation_status_img = db.Column(db.String(2000), nullable =False, primary_key = True)

#create class for biome
class Biome(db.Model):
	__tablename__ = 'biome'
	biome_type = db.Column(db.String(80), nullable = False, primary_key = True)
	biome_desc = db.Column(db.String(2000), nullable =False, primary_key = True)
	biome_img = db.Column(db.String(2000), nullable =False, primary_key = True)
	animal = db.Column(db.String(80), nullable = False, primary_key = True)
	average_temperature = db.Column(db.String(80), nullable = False, primary_key = True)

#create a class for class
class Class(db.Model):
	__tablename__ = 'classes'
	class_type = db.Column(db.String(80), nullable = False, primary_key = True)
	class_desc = db.Column(db.String(2000), nullable =False, primary_key = True)
	class_wiki_url = db.Column(db.String(2000), nullable =False, primary_key = True)
	class_img_url = db.Column(db.String(2000), nullable =False, primary_key = True)
	parent_phylum = db.Column(db.String(80), nullable = False, primary_key = True)

#create a class for order
class Order(db.Model):
	__tablename__ = 'orders'
	order_type = db.Column(db.String(80), nullable = False, primary_key = True)
	order_desc = db.Column(db.String(2000), nullable =False, primary_key = True)
	order_wiki_url = db.Column(db.String(2000), nullable =False, primary_key = True)
	order_img_url = db.Column(db.String(2000), nullable =False, primary_key = True)
	parent_class = db.Column(db.String(80), nullable = False, primary_key = True)

#create a class for family
class Family(db.Model):
	__tablename__ = 'families'
	family_type = db.Column(db.String(80), nullable = False, primary_key = True)
	family_desc = db.Column(db.String(2000), nullable =False, primary_key = True)
	family_wiki_url = db.Column(db.String(2000), nullable =False, primary_key = True)
	family_img_url = db.Column(db.String(2000), nullable =False, primary_key = True)
	parent_order = db.Column(db.String(80), nullable = False, primary_key = True)

#create a class for conservation
class Conservation(db.Model):
	__tablename__ = 'conservation'
	conservation_status = db.Column(db.String(80), nullable = False, primary_key = True)
	conservation_status_wiki_url = db.Column(db.String(2000), nullable =False, primary_key = True)
	conservation_status_desc = db.Column(db.String(2000), nullable =False, primary_key = True)
	conservation_status_img = db.Column(db.String(2000), nullable =False, primary_key = True)
	animal = db.Column(db.String(80), nullable = False, primary_key = True)
db.drop_all()
db.create_all()
# End of models.py
