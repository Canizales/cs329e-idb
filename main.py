from flask import Flask, render_template, request
from create_animal import app, db, Animal, create_animals, Class, Order, Family, Conservation, Biome


@app.route('/')
def home():
  return render_template('home.html')

@app.route('/animals')
def animals():
  page = request.args.get('page',1, type=int)
  animals = db.session.query(Animal).paginate(page=page,per_page=8)
  #animals = db.session.query(Animal).all()
  return render_template('animals.html', animals = animals)

@app.route('/biome')
def biome():
  page = request.args.get('page',1, type=int)
  biomes = db.session.query(Biome).paginate(page=page, per_page=6)
  return render_template('biome.html', biomes = biomes)

@app.route('/taxonomy')
def taxonomy():
  return render_template('taxonomy.html')

@app.route('/class_type')
def class_type():
  page = request.args.get('page',1, type=int)
  classes = db.session.query(Class).paginate(page=page,per_page=8)
  return render_template('class_type.html', classes = classes)

@app.route('/order_type')
def order_type():
  page = request.args.get('page',1, type=int)
  orders = db.session.query(Order).paginate(page=page,per_page=8)
  return render_template('order_type.html', orders = orders)

@app.route('/family_type')
def family_type():
  page = request.args.get('page',1, type=int)
  families = db.session.query(Family).paginate(page=page,per_page=8)
  return render_template('family_type.html', families = families)

@app.route('/conservation')
def conservation():
  page = request.args.get('page',1, type=int)
  statuses = db.session.query(Conservation).paginate(page=page,per_page=8)
  return render_template('conservation.html', statuses = statuses)

@app.route('/about')
def about():
  return render_template('about.html')

if __name__ == '__main__':

  app.run()
