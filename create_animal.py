import json

#from flask import Flask
#from flask_sqlalchemy import SQLAlchemy
from models import app, db, Animal, Class, Order, Family, Conservation, Biome

#import os

#app = Flask(__name__)
#app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:2512488@localhost:5432/animaldb')
#app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
#db = SQLAlchemy(app)


def load_json(filename):
    with open(filename, encoding="utf8") as file:
        jsn = json.load(file)
        file.close()

    return jsn

#a function to exact animal from json file
def create_animals():
    animal = load_json('data.json')
    index = 0
    for oneAnimal in animal['animal']:
        animal = oneAnimal["animal's name"]
        scientific_name = oneAnimal['animal scientific name']
        image = oneAnimal['image_url']
        description= oneAnimal['description']
        x = oneAnimal['biome'][0]
        biome = x['biome type']
        biome_wiki_url = x['wiki url']
        biome_desc = x['description']
        biome_img = x['image_url']
        y = oneAnimal['classification']
        phylum = y[0]
        classT = y[1]
        order = y[2]
        family = y[3]
        class_type = classT["class type"]
        phylum_type = phylum["phylum type"]
        class_type = classT["class type"]
        order_type = order["order type"]
        family_type = family["family type"]
        z = oneAnimal['conservation status'][0]
        conservation_status = z["status type"]
        conservation_status_desc = z["description"]
        conservation_status_wiki_url = z["wiki url"]
        conservation_status_img = z["image_url"]


        newAnimal = Animal(animal=animal, scientific_name = scientific_name, image=image, description=description,biome=biome,
        biome_wiki_url=biome_wiki_url, biome_desc=biome_desc, biome_img=biome_img,phylum_type = phylum_type, class_type = class_type, order_type=order_type
        ,family_type = family_type,conservation_status=conservation_status,conservation_status_wiki_url=conservation_status_wiki_url,
        conservation_status_desc=conservation_status_desc,conservation_status_img=conservation_status_img)

        # After I create the animal, I can then add it to my session.
        db.session.add(newAnimal)
        # commit the session to my DB.
        db.session.commit()

#create a function to exact only the unique biome 
def create_biome():
    animal = load_json('data.json')
    index = 0
    #a dictionary to have unique biome
    biomes = set()
    for oneAnimal in animal['animal']:
        x = oneAnimal['biome'][0]
        biome_type = x['biome type']
        biome_desc = x['description']
        biome_img = x['image_url']
        average_temperature = x['average temperature']
        animal = oneAnimal["animal's name"]

        if (biome_type not in biomes):
            newBiome = Biome(biome_type=biome_type,biome_desc=biome_desc,biome_img=biome_img,animal=animal,average_temperature=average_temperature)
            db.session.add(newBiome)
            db.session.commit()
            biomes.add(biome_type)
        index +=1


#create a function for class
def create_class():
    animal = load_json('data.json')
    index = 0
    classes = set()
    for oneAnimal in animal['animal']:
        y = oneAnimal['classification']
        parent_phylum = y[0]["phylum type"]
        classT = y[1]
        class_type = classT["class type"]
        class_desc = classT["description"]
        class_wiki_url = classT["wiki url"]
        class_img_url = classT["image_url"]
        if (class_type not in classes):
            newClass = Class(class_type=class_type,class_desc=class_desc, class_wiki_url=class_wiki_url,class_img_url=class_img_url, parent_phylum =parent_phylum)
            db.session.add(newClass)
            db.session.commit()
            classes.add(class_type)
        index +=1
#create a function for uniqur order
def create_order():
    animal = load_json('data.json')
    index = 0
    #similar to biome
    orders = set()
    for oneAnimal in animal['animal']:
        y = oneAnimal['classification']
        parent_class = y[1]["class type"]
        order = y[2]
        order_type = order["order type"]
        order_desc = order["description"]
        order_wiki_url = order["wiki url"]
        order_img_url = order["image_url"]

        if (order_type not in orders):
            newOrder = Order(order_type=order_type,order_desc=order_desc, order_wiki_url=order_wiki_url, order_img_url=order_img_url, parent_class=parent_class)
            db.session.add(newOrder)
            db.session.commit()
            orders.add(order_type)
        index +=1

#create a function for unique family
def create_family():
    animal = load_json('data.json')
    index = 0
    families = set()
    for oneAnimal in animal['animal']:
        y = oneAnimal['classification']
        parent_order = y[2]["order type"]
        family = y[3]
        family_type = family["family type"]
        family_desc = family["description"]
        family_wiki_url = family["wiki url"]
        family_img_url = family["image_url"]
        if (family_type not in families):
            newFamily = Family(family_type=family_type,family_desc=family_desc,family_wiki_url=family_wiki_url, family_img_url=family_img_url, parent_order=parent_order)
            db.session.add(newFamily)
            db.session.commit()
            families.add(family_type)
        index +=1
#create a function for unique conservation
def create_conservation():
    animal = load_json('data.json')
    statuses = set()
    index = 0
    for oneAnimal in animal['animal']:
        z = oneAnimal['conservation status'][0]
        conservation_status = z["status type"]
        animal = oneAnimal["animal's name"]
        if (conservation_status not in statuses):
            conservation_status_desc = z["description"]
            conservation_status_wiki_url = z["wiki url"]
            conservation_status_img = z["image_url"]
            newConservation = Conservation(conservation_status=conservation_status,conservation_status_wiki_url=conservation_status_wiki_url,conservation_status_desc=conservation_status_desc,conservation_status_img=conservation_status_img,animal=animal)
            db.session.add(newConservation)
            db.session.commit()
            statuses.add(conservation_status)
        index += 1

create_animals()
create_conservation()
create_class()
create_order()
create_family()
create_biome()
